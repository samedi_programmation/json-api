############################
# Les Samedi Programmation #
############################


Samedi 04/07/2020 : Interfaces JSON manuelles
=============================================


Ce code source permet de montrer comment s'interfacer avec les API REST JSON des sites OMDBAPI & RESTCOUNTRIES pour récupérer les informations d'un film, et le drapeau des pays du film.

API exploitées : https://www.omdbapi.com/ & https://restcountries.eu/


- Comment accéder à une page Web
- Comment créer une structure objet (avec objets imbriqués)
- Comment traduire une chaîne de caractères en JSON
- Comment découper une chaîne de caractères en tableau
- Comment recomposer un tableau en une chaîne de caractères
- Comment lire un fichier texte
- Comment remplacer des chaînes de caractères
- Comment écrire un fichier texte
- Comment nettoyer une chaîne de caractères pour n'avoir que les types de caractères souhaités (lettres, chiffres)

Pour compiler et obtenir un exécutable, go build main.go

Si vous avez plus de questions, vous pouvez me contacter sur https://twitch.tv/adaralex

Bonne journée à vous,

Adaralex