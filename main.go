package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"unicode"
)

type OmdbRatings struct {
	Source string `json:"Source"`
	Valeur string `json:"Value"`
}

type OmdbResponse struct {
	Titre            string        `json:"Title"`
	Annee            string        `json:"Year"`
	Duree            string        `json:"Runtime"`
	Classification   string        `json:"Rated"`
	Sortie           string        `json:"Released"`
	Genre            string        `json:"Genre"`
	Directeur        string        `json:"Director"`
	Auteur           string        `json:"Writer"`
	Acteurs          string        `json:"Actors"`
	Intrigue         string        `json:"Plot"`
	Langue           string        `json:"Language"`
	Pays             string        `json:"Country"`
	Recompenses      string        `json:"Awards"`
	Affiche          string        `json:"Poster"`
	Scores           []OmdbRatings `json:"Ratings"`
	Metascore        string        `json:"Metascore"`
	Score_IMDB       string        `json:"imdbRating"`
	Votes_IMDB       string        `json:"imdbVotes"`
	Identifiant_IMDB string        `json:"imdbID"`
	Type             string        `json:"Type"`
	Sortie_DVD       string        `json:"DVD"`
	BoxOffice        string        `json:"BoxOffice"`
	Production       string        `json:"Production"`
	Site_Web         string        `json:"Website"`
	Reponse          string        `json:"Response"`
}

type RestCountryLanguages struct {
	ISO_639_1 string `json:"iso_639_1"`
	ISO_639_2 string `json:"iso_639_2"`
	Name string `json:"name"`
	NativeName string `json:"native_name"`
}

type RestCountries struct {
	Name string `json:"name"`
	Capital string `json:"capital"`
	Region string `json:"region"`
	Population int64 `json:"population"`
	Location []float32 `json:"latlng"`
	Area float64 `json:"area"`
	Languages []RestCountryLanguages `json:"languages"`
	Flag string `json:"flag"`
}


func RecupereDrapeauPays(pays string) string {

	url_movie := fmt.Sprintf("https://restcountries.eu/rest/v2/name/%s", pays)

	resp, err := http.Get(url_movie)

	if err != nil {
		log.Fatalln(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	resultat := []RestCountries{}

	if err := json.Unmarshal(body, &resultat); err != nil {
		panic(err)
	}

	if len(resultat) != 1 {
		return pays
	} else {
		return fmt.Sprintf("<img src=\"%s\" width=\"50\"/>", resultat[0].Flag)
	}
}

// Pour obtenir votre clé API OMDBAPI, allez sur https://www.omdbapi.com/

func GetApiKey() string {
    return ""
}

func main() {

	movie := "Star Trek Beyond"



	url_movie := fmt.Sprintf("http://www.omdbapi.com/?apikey=%s&t=%s", GetApiKey(), url.QueryEscape(movie))

	resp, err := http.Get(url_movie)

	if err != nil {
		log.Fatalln(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	log.Println(string(body))

	resultat := OmdbResponse{}

	if err := json.Unmarshal(body, &resultat); err != nil {
		panic(err)
	}

	fmt.Printf("Le film %s est sorti en %s ; pour le regarder, prévoyez %s\n", resultat.Titre, resultat.Annee, resultat.Duree)

	listePays := strings.Split(resultat.Pays, ",")

	listeDrapeauxPays := make([]string, 0)


	for _, pays := range listePays {
		listeDrapeauxPays = append(listeDrapeauxPays, RecupereDrapeauPays(strings.Trim(pays, " ")))
	}

	html_file, err := ioutil.ReadFile("fiche_film.html")

	if err != nil {
		log.Fatalln(err)
	}

	sortie_html := string(html_file)


	sortie_html = strings.ReplaceAll(sortie_html, "%%movie%%", resultat.Titre)
	sortie_html = strings.ReplaceAll(sortie_html, "%%movie_poster%%", resultat.Affiche)
	sortie_html = strings.ReplaceAll(sortie_html, "%%movie_release_date%%", resultat.Sortie)
	sortie_html = strings.ReplaceAll(sortie_html, "%%movie_genre%%", resultat.Genre)
	sortie_html = strings.ReplaceAll(sortie_html, "%%movie_plot%%", resultat.Intrigue)
	sortie_html = strings.ReplaceAll(sortie_html, "%%movie_actors%%", resultat.Acteurs)
	sortie_html = strings.ReplaceAll(sortie_html, "%%movie_country%%", strings.Join(listeDrapeauxPays, ", "))
	sortie_html = strings.ReplaceAll(sortie_html, "%%movie_language%%", resultat.Langue)


	log.Println(sortie_html)

	nom_fichier := ""

	for _, c := range resultat.Titre {
		 if unicode.IsLetter(c) || unicode.IsNumber(c) {
		 	nom_fichier = nom_fichier + string(c)
		 }
	}

	f, err := os.Create(nom_fichier + ".html")
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	w := bufio.NewWriter(f)
	w.WriteString(sortie_html)
	w.Flush()

}
